<?php
return [
   "title" => '零艺客API',
   "name" => '提供免费接口调用平台',
   "keywords" => 'API,聚合数据,API数据接口,API,免费接口,免费api接口调用,免费API数据调用,零艺客API,零艺API',
   "description" => '零艺客API是零艺免费提供API数据接口调用服务平台 - 我们致力于为用户提供稳定、快速的免费API数据接口服务。',
   "qq" => '656001878',
   "time" => '2019/02/14 18:00:00',
   "template" => 'youqi',
   "emailRegister" => '1',
   "url1" => 'https://cdn.item.lessh.cn/img/z.png',
   "url2" => 'https://cdn.item.lessh.cn/img/w.jpg',
   "email" => 'zeroart@qq.com',
   "icp" => '<span>© 2017-2019</span><a href="http://www.miibeian.gov.cn/"                      target="_blank">桂ICP备xxxxxxxx</a><span></span>',
   "about" => '<h4>关于 <a href="http://lessh.cn" target="_blank">ZeroArt</a> API</h4>
                <p>
                    ZeroArt API 是 <a href="http://www.lykep.com.com/" target="_blank">零艺</a>
                    支持并维护的 API 接口项目，致力于为用户提供稳定、快速的免费 API 接口服务平台。
                </p>
                <p>
                    反馈或建议请发送邮件至：admin@qq.com
                </p>',
   "gg" => '<p style="text-indent: 2em;"><span style="color:red">正式发布</span></p>
    <p style="text-indent: 2em;">鉴于之前有部分用户对本站进行一些注入渗透等等入侵行为，后期本站会开启注册，接口调用者需要前往后台生成专属的KEY</p>
    <p style="text-indent: 2em;">提供的公益接口服务，零盈利。为了长久、稳定的发展，且行且珍惜。</p>
    <p style="text-indent: 2em;">3. 有任何问题联系QQ：656001878</p><span
        style="color:red">使用者务必遵循法律法规，切勿滥用，使用爬虫等<br />否则封禁KEY、IP以及账号</span></center>
    </p>',
   "target" => 'webinfo',
];
?>