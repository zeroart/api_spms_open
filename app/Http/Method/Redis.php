<?php
class RInterceptor
{

  public $redis;

  public $cof;

  // 初始化
  public function init()
  {
    $this->cof = config('redis');
    //实例化一个Redis
    $this->redis = new Redis();
    //Redis 连接
    $this->redis->connect($this->cof['redis_url'], $this->cof['redis_port']);
    if (!empty($this->cof['redis_pass'])) $this->redis->auth($this->cof['redis_pass']); //密码验证
    if ($this->redis->ping() != '+PONG') {
      exit("Redis异常");
    }
    return $this;
  }

  // 检查
  public function openCheck($key)
  {
    //获取一个key
    $check = $this->redis->exists($key);
    //判断key是否存在
    if ($check) {
      //将key中储存的数字值增一。如果key不存在，以0为key的初始值
      $this->redis->incr($key);
      //返回key所关联的字符串值。如果key不存在，返回nil。
      $count = $this->redis->get($key);
      //判断周期内的执行次数 每小时只能执行10次
      if ($count > $this->cof['redis_next2']) {
        exit(json_encode(["code" => 203, "msg" => "调试次数上限，每" . $this->cof['redis_period2'] . "秒" . $this->cof['redis_next2'] . "次。"], 320));
      }
    } else {
      //将key中储存的数字值增一。如果key不存在，以0为key的初始值
      $this->redis->incr($key);
      //设置生命周期为x秒 每小时
      $this->redis->expire($key, $this->cof['redis_period2']);
    }
    //返回key所关联的字符串值。如果key不存在，返回nil。
    return $this->redis->get($key);
  }

  // 检查
  public function check($key)
  {
    if (!isset($key)) {
      //获取客户端IP
      $key = get_real_ip();
    }
    //获取一个key
    $check = $this->redis->exists($key);
    //判断key是否存在
    if ($check) {
      //将key中储存的数字值增一。如果key不存在，以0为key的初始值
      $this->redis->incr($key);
      //返回key所关联的字符串值。如果key不存在，返回nil。
      $count = $this->redis->get($key);
      //判断周期内的执行次数
      if ($count > $this->cof['redis_next']) {
        exit($this->cof['redis_msg']);
      }
    } else {
      //将key中储存的数字值增一。如果key不存在，以0为key的初始值
      $this->redis->incr($key);
      //设置生命周期为x秒
      $this->redis->expire($key, $this->cof['redis_period']);
    }
    //返回key所关联的字符串值。如果key不存在，返回nil。
    return $this->redis->get($key);
  }

  // 设置
  public function set($key, $data, $time = false)
  {
    $res = $this->redis->set($key, $data);
    if ($time) {
      $this->redis->expire($key, $this->cof['redis_period']);
    }
    return $res;
  }

  // 获取
  public function get($key)
  {
    return $this->redis->get($key);
  }
}
